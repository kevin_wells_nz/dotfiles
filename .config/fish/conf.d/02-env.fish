# Logic based on [systemd-detect-virt](https://github.com/systemd/systemd/blob/496b4fa0e974d7a1b10b8af966da445a28f512c5/src/basic/virt.c#L674)
set container ''
if test -f /.dockerenv
    set container 'docker'
else if test -f /run/.containerenv
    set container 'podman'
else if test -f /proc/sys/kernel/osrelease && grep 'WSL' /proc/sys/kernel/osrelease
    set container 'wsl'
end

if test "$container" != ""
    # Don't use FUSE when run in a container.
    # See https://github.com/AppImage/AppImageKit/issues/912#issuecomment-528669441
    export APPIMAGE_EXTRACT_AND_RUN=1
end
