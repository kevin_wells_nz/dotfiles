if type fzf &> /dev/null;
	# Using `fd` is much faster than `find` (the default)
	# because it respects `.gitignore`.
	# See https://github.com/junegunn/fzf?tab=readme-ov-file#tips`
	if type fd &> /dev/null;
		set FZF_DEFAULT_COMMAND 'fd . --strip-cwd-prefix --hidden --type f'
		set FZF_CTRL_T_COMMAND 'fd . --strip-cwd-prefix --hidden --type f --type d'
		set FZF_ALT_C_COMMAND 'fd . --strip-cwd-prefix --hidden --type d'
	end

	set FZF_DEFAULT_OPTS '-i --height=50%'

	# Set up fzf key bindings and fuzzy completion
	eval "$(fzf --fish)"
end
