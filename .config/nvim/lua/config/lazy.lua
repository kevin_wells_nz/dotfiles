-- https://lazy.folke.io/installation Structured  -- plugins, opts)Setup
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	local lazyrepo = "https://github.com/folke/lazy.nvim.git"
	vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
end
vim.opt.rtp:prepend(lazypath)

-- Globals
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Options
vim.opt.ignorecase = true
vim.opt.smartcase = true

vim.opt.list = true
vim.opt.listchars = { tab = "» ", trail = "·", nbsp = "␣" }

vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.hlsearch = false

vim.opt.colorcolumn = "81"

-- lazy.nvim
require("lazy").setup({
	spec = {
		{ import = "plugins" },
	},
	-- install = { colorscheme = { "habamax" } },
	-- checker = { enabled = true },
})
