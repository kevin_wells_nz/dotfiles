return {
	{
		"catppuccin/nvim",
		name = "catppuccin",
		priority = 1000,
		config = function()
			require("catppuccin").setup({
				highlight_overrides = {
					all = function()
						return {
							LineNr = { fg = "#eeeeee" },
							LineNrAbove = { fg = "#999999" },
							LineNrBelow = { fg = "#999999" },
						}
					end,
				},
			})

			vim.cmd.colorscheme("catppuccin-mocha")
		end,
	},
}
