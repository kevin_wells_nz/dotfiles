# Dotfiles

Install dotfiles on a new host using this command:

    curl -sL https://gitlab.com/kevin_wells_nz/dotfiles/-/raw/main/.local/bin/install | bash

This also installs `git` and `podman`, runs `~/.local/bin/install.d/dev-user`
to installs CLI tools, and optionally runs a host specific installation
script e.g. `~/.local/bin/install.d/hermes`.

## Working with dot files

Dotfiles are kept in a bare `$HOME/.dotfiles` git repository with `$HOME`
as it's working directory. Use it like this:

    dotfiles status
    dotfiles add .config/my/config
    dotfiles commit -m 'add my config"
    dotfiles push

See [Dotfiles: Best way to store in a bare git repository](https://www.atlassian.com/git/tutorials/dotfiles)

Both `chezmoi` and `stow` are popular but I think a bare git
repository is simpler and sufficient for now.

## User tools

The `~/.local/bin/install.d/dev-user` scripts installs programs that I use
on the host and in containers. Where possible `musl` programs are installed
to avoid issues with different `libc` versions on the host and in containers.
Typically very recent versions of each program are installed rather than
the version provided with the distro.

## User space in containers

I'm running most of user space in `distrobox` containers configured with `.ini`
files in the `.local/bin/install.d/hermes.d` directory.

When a terminal is opened on the host by default it will open a CLI container.
This has my dotfiles, shell, editor, etc. From the CLI container I can attach
to development containers.

This makes it easy to setup the same environment on multiple machines and
to reprovision a machine after a hardware failure. The approach is based
on the following articles.

- [Toolboxes are not just for special cases](https://www.ypsidanger.com/toolboxes-are-not-just-for-special-cases/)
- [Erase your darlings](https://grahamc.com/blog/erase-your-darlings/)

## Dev tools

I'm using [mise](https://mise.jdx.dev/) to automatically install specific
versions of dev tools like `node` when switching to a project directory.
Together with a package manager this provides a reproducible build
environment for many projects.

## Dev dependencies

Projects that depend on libraries installed in the OS are installed with a
`dev` script. I base the `cli`  container on `debian` and use this for
development builds of native applications. Just run `dev` to install
dependencies before building the project. This is much faster and simpler
than using a container for each project.

## Dev containers

Dev tools like C that build native executables depend on environment
specific libraries and header files. These dependencies are captured
in a project specific dev container.

Each project has a `dev` script that builds and runs a docker container.
Run the script and do devlopment work within the container.
Specify dependent tools and `packages` in the `dev` script.
The container runs as the host user, with the current directory as
work directory. The host home directory is mounted in the container
to provide access to tools in `~/.local/bin` on the host.

    #!/bin/bash

    packages="git build-essential"

    user="${USER:-user}"
    uid="$(id -u)"
    gid="$(id -g)"

    podman build --quiet --pull=newer -t work-zig -f Dockerfile \
        --build-arg=packages="$packages" \
        --build-arg=user="$user" \
        --build-arg=uid="$uid" \
        --build-arg=gid="$gid" \

    podman run \
        --userns=keep-id \
        -v /home/kevin:"/home/${user}":rslave \
        -v "$(pwd)":/work:rslave \
        --rm -it work-zig

The `Dockerfile` installs packages and sets up a user with a work directory.

    FROM debian

    ARG packages
    ARG user=user
    ARG uid=1000
    ARG gid=1000

    RUN apt-get update \
        && apt-get -y upgrade \
        && apt-get install -y ${packages} \
        && apt-get clean

    RUN addgroup --gid "$gid" "$user" \
        && adduser --uid "$uid" --gid "$gid" --disabled-password --gecos "" "$user" \
        && echo "$user ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers \
        && mkdir /work \
        && chown "$user":"$user" /work

    USER $user
    WORKDIR "/work"

# GUI dev containers

For GUI projects I use `dev` script that creates a container using
[Distrobox](https://github.com/89luca89/distrobox) for access to
the host GUI.

    #!/bin/bash

    distrobox assemble create --replace --file "distrobox.ini"
    distrobox enter -n box-odin

Required packages are specified in a `distrobox.ini` file passed to
[Distrobox assemble](https://github.com/89luca89/distrobox/blob/main/docs/usage/distrobox-assemble.md).

    [box-odin]
    additional_packages="git build-essential clang"
    additional_packages="libsdl2-dev"
    # additional_packages="libsdl2-2.0-0"

    image=debian:latest
    pull=true

# Hermes

Hermes is my gaming machine. All of user space runs in `distrobox`. This machine
should be reprovisioned using this command:

    curl -sL https://gitlab.com/kevin_wells_nz/dotfiles/-/raw/main/.local/bin/hermes.sh | bash

See https://www.ypsidanger.com/toolboxes-are-not-just-for-special-cases

# Reproducible dev environments

When starting work on an existing project it should be really easy to
install project development tools so you can start work immediately.
It might look someting like this:

    $ git clone https://myproject.git
    $ cd myproject
    $ ./dev
    (dev) $ New environment with build tools for myproject e.g. `go`

The environment should also be setup based on user preferences, so
each developer can use their preferred editor, shell, and other tools.

To enable this we need the following

- Project configuration
- User configuration
- Code that sets up the environment

## Containers

Project configuration can be described using a Dockerfile that is
used to build an image with build tools.

    FROM docker.io/library/debian:12.7 # or docker.io/library/golang
    RUN # install project tools e.g. `go`
    RUN "$HOME/.local/bin/dev.sh"

The last line adds a script provided by the user to install their
preferred tools.

## Tool installation

Tools can often be installed using a package manager, but this isn't
always an option.

- A distribution may not provide a package.
- The package provided by a distribution may be too old.
- When using multiple distributions different package versions
  may be provided by each distribution. This will cause issues
  if different versions of the program share data that is not
  forward compatible.

Essentially this requires using someting other than a
distribution package manager to install some tools. There are
a few options for this.

- [Homebrew](https://docs.brew.sh/Homebrew-on-Linux)
- [GPM](https://github.com/aerys/gpm)
  or [Install Release](https://github.com/Rishang/install-release)
  or write my own tool to install Git releases.

NB: Alternatively this requires living with an old version of
the tool in some containers.

## Updates

Containers need to be updated for security and simply to keep up
with upstream. We should also identify where a container is
out of date because upstream or a file it depends on has changed
(e.g. user configuration).

Tools installed with something other that a package manager also
need to be updated.

TODO: Script this

## Dynamic libraries

Programs that are not installed using a distribution package
manager may depend on shared libraries that are no installed
on that distribution. The simplest fix for this is to
prefer programs that are statically linked against musl
libc, which often means choosing tools written in Go or Rust.

## Other containers

Other containers may run on the host to provide CLI and GUI programs.
It is useful to share user configuration used for projects with
the CLI container.

# Reproducible machines

Extend approach above.



This is easy if I use the distro for development. It is more complex
if different distros are used because of different package managers,
and different shared library versions.

Stable Debian is a popular choice for development images. Debian stable
packages are typically outdated. Debian stable has a relatively old
with stable Debian is packages are often outdated. Alpine is appealing
but some things may not work because it uses musl.


## Dynamic linking

Programs written in C on Linux are dynamically linked against shared libraries in the system
they were built on. The system they run on must have compatible libraries installed.

# WSL

## GitHub auth

I use the following for GitHub auth within WSL

    BROWSER="/mnt/c/Program\ Files/Google/Chrome/Application/chrome.exe" gh auth login

I also setup GCM as described below. I'm not sure this is required, but
it makes sense to store credentials securely in Windows.

This assumes when re running in WSL that GCM was installed for Windows
by installing the latest Git for Windows e.g. `winget install -id Git.Git -e --source winget`

    git config --global credential.helper "/mnt/c/Program\ Files/Git/mingw64/bin/git-credential-manager.exe"

See https://learn.microsoft.com/en-us/windows/wsl/tutorials/wsl-git
