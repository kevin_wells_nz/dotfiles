#!/usr/bin/env bash

# Run `distrobox assemble create` on each `.ini` in the current directory that
# has changed. Changes are detected using a `.{file}.sha256` file.
#
# Use separate `.ini` files for containers that are expensive to create or
# change frequently.
#
# See https://github.com/89luca89/distrobox/blob/main/docs/usage/distrobox-assemble.md
for file in *.ini; do
	changed=false
	if [ ! -f ".${file}.sha256" ] || ! sha256sum -c --status ".${file}.sha256"; then
		changed=true
	fi

	if $changed; then
		echo "creating ${file}"

		distrobox assemble create --replace --file "${file}"

		if [ $? -eq 0 ]; then
			sha256sum "${file}" > ".${file}.sha256"
		else
			echo 'error'
		fi
	fi
done

