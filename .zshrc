# ~/.zshrc

PATH="$HOME/.local/bin:$PATH"

if type starship &> /dev/null; then
	eval "$(starship init zsh)"
fi

if type fzf &> /dev/null; then
	# Set up fzf key bindings and fuzzy completion
	eval "$(fzf --zsh)"

	# Using `fd` is much faster than `find` (the default)
	# because it respects `.gitignore`.
	# See https://github.com/junegunn/fzf?tab=readme-ov-file#tips`
	if type fd &> /dev/null; then
		export FZF_DEFAULT_COMMAND='fd . --strip-cwd-prefix --hidden --type f'
		export FZF_CTRL_T_COMMAND='fd . --strip-cwd-prefix --hidden --type f --type d'
		export FZF_ALT_C_COMMAND='fd . --strip-cwd-prefix --hidden --type d'
	fi

	export FZF_DEFAULT_OPTS='-i --height=50%'
fi

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
