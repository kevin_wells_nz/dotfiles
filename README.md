# Dotfiles

Install dotfiles on a new host using this command:

    curl -sL https://gitlab.com/kevin_wells_nz/dotfiles/-/raw/main/.local/bin/install | bash

See [Install](.local/bin/README.md) documentation.

## Working with dot files

Dotfiles are kept in a bare `$HOME/.dotfiles` git repository with `$HOME`
as it's working directory. Use it like this:

    dotfiles status
    dotfiles add .config/my/config
    dotfiles commit -m 'add my config"
    dotfiles push

See [Dotfiles: Best way to store in a bare git repository](https://www.atlassian.com/git/tutorials/dotfiles)

Both `chezmoi` and `stow` are popular but I think a bare git
repository is simpler and sufficient for now.
